package com.nespresso.recruitment.gossip.parser;

public interface IGossipsParser {
	public String[] parseGarrulous(String person_);
}
