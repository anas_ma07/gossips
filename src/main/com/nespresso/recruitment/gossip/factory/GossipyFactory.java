package com.nespresso.recruitment.gossip.factory;

import com.nespresso.recruitment.gossip.person.AbstractPerson;
import com.nespresso.recruitment.gossip.person.PersonAgent;
import com.nespresso.recruitment.gossip.person.PersonDr;
import com.nespresso.recruitment.gossip.person.PersonMr;
import com.nespresso.recruitment.gossip.person.PersonPr;

public enum GossipyFactory {

	INSTANCE;

	public AbstractPerson createGarrulous(String[] personTypeName){
		String personType = personTypeName[0];
		String personName = personTypeName[1];
		switch (personType) {
		case "Mr":
			return new PersonMr(personName);
		case "Dr":
			return new PersonDr(personName);
		case "Agent":
			return new PersonAgent(personName);
		case "Pr":
			return new PersonPr(personName);

		default:
			throw new IllegalArgumentException();
		}
	}
}
