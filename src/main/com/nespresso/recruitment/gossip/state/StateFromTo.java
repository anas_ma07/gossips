package com.nespresso.recruitment.gossip.state;

import com.nespresso.recruitment.gossip.person.AbstractPerson;

public class StateFromTo implements State {

	private AbstractPerson personSource;

	public StateFromTo(AbstractPerson personSource) {
		super();
		this.personSource = personSource;
	}

	@Override
	public void to(AbstractPerson person) {
		person.addSource(personSource);
	}

}
