package com.nespresso.recruitment.gossip.state;

import com.nespresso.recruitment.gossip.person.AbstractPerson;

public interface State {

	public void to(AbstractPerson person);
}
