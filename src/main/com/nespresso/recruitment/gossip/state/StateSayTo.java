package com.nespresso.recruitment.gossip.state;

import com.nespresso.recruitment.gossip.person.AbstractPerson;

public class StateSayTo implements State {

	private String message;

	public StateSayTo(String message) {
		super();
		this.message = message;
	}

	@Override
	public void to(AbstractPerson person) {
		person.initializeMessage(message);
	}

}
