package com.nespresso.recruitment.gossip.person;

public class PersonPr extends PersonMr {

	private int numberOfRequestToSend = 0;

	public PersonPr(String name) {
		super(name);
	}

	@Override
	protected String sendMessage() {
		String messageTosend = "";
		if (numberOfRequestToSend > 0){
			messageTosend = new String(this.messageTosay);
			this.messageTosay = "";
			numberOfRequestToSend = -1;
		}
		numberOfRequestToSend++;
		return messageTosend;
	}

}
