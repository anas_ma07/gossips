package com.nespresso.recruitment.gossip.person;

public class PersonMr extends AbstractPerson {

	private String messageRecieved = "";
	protected String messageTosay = "";

	public PersonMr(String name) {
		super(name);
	}

	@Override
	public void recieveMessage() {
		for (AbstractPerson sourcePerson : sources) {
			if (sourcePerson.speak() != "") {
				this.messageRecieved = sourcePerson.sendMessage();
				return;
			}
		}
	}

	@Override
	public void synchronize() {
		this.messageTosay = (this.messageRecieved == "" ? this.messageTosay
				: this.messageRecieved);
		this.messageRecieved = "";
	}

	@Override
	public String speak() {
		return this.messageTosay;
	}

	@Override
	public void initializeMessage(String message) {
		this.messageTosay = message;
	}

	@Override
	protected String sendMessage() {
		String messageTosend = new String(this.messageTosay);
		this.messageTosay = "";
		return messageTosend;
	}

}
