package com.nespresso.recruitment.gossip.person;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractPerson {
	protected String name;
	protected List<AbstractPerson> sources;

	public AbstractPerson(String name) {
		super();
		this.sources = new ArrayList<AbstractPerson>();
		this.name = name;
	}

	public abstract void recieveMessage();

	public abstract void synchronize();

	public abstract String speak();
	
	public abstract void initializeMessage(String message);

	public void addSource(AbstractPerson personSource) {
		this.sources.add(personSource);
	}

	protected abstract String sendMessage();
}
