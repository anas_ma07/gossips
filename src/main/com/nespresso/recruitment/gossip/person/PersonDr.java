package com.nespresso.recruitment.gossip.person;

import java.util.ArrayList;
import java.util.List;

public class PersonDr extends AbstractPerson {

	protected List<String> messageRecieveds = new ArrayList<String>();
	protected List<String> messageTosay = new ArrayList<String>();
	private static final String SEPARATOR = ", ";
	protected int indexOfMessage = 0;

	public PersonDr(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void recieveMessage() {
		for (AbstractPerson sourcePerson : sources) {
			if (sourcePerson.speak() != "") {
				this.messageRecieveds.add(sourcePerson.sendMessage());
			}
		}
	}

	@Override
	public void synchronize() {
		if (indexOfMessage + 1 > this.messageRecieveds.size())
			return;
		this.messageTosay = (this.messageRecieveds == null ? this.messageTosay
				: this.messageRecieveds);
		this.messageRecieveds = new ArrayList<String>();
	}

	@Override
	public String speak() {
		StringBuilder message = new StringBuilder();
		for (String message_ : this.messageTosay) {
			message.append(message_);
			message.append(SEPARATOR);
		}
		return (message.length() > 3 ? message.delete(message.length() - 2,
				message.length()).toString() : "");
	}

	@Override
	public void initializeMessage(String message) {
		this.messageTosay.add(message);
	}

	@Override
	protected String sendMessage() {
		String messageTosend = "";
		if (this.messageTosay.size() > 0) {
			messageTosend = this.messageTosay.get(this.indexOfMessage);
			this.indexOfMessage++;
		}

		return messageTosend;
	}

}
