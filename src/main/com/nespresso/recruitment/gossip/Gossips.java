package com.nespresso.recruitment.gossip;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.nespresso.recruitment.gossip.factory.GossipyFactory;
import com.nespresso.recruitment.gossip.parser.GossipsParser;
import com.nespresso.recruitment.gossip.parser.IGossipsParser;
import com.nespresso.recruitment.gossip.person.AbstractPerson;
import com.nespresso.recruitment.gossip.state.State;
import com.nespresso.recruitment.gossip.state.StateFromTo;
import com.nespresso.recruitment.gossip.state.StateSayTo;

public class Gossips {

	private Map<String, AbstractPerson> persons;
	private State stateOfTo;

	public Gossips(String... persons) {
		this.persons = new LinkedHashMap<String, AbstractPerson>();
		String[] personTypeName;
		IGossipsParser gossipsParser = new GossipsParser();
		for (String person : persons) {
			personTypeName = gossipsParser.parseGarrulous(person);
			this.persons.put(personTypeName[1],
					GossipyFactory.INSTANCE.createGarrulous(personTypeName));
		}
	}

	public Gossips from(String personName) {
		AbstractPerson from = persons.get(personName);
		this.stateOfTo = new StateFromTo(from);
		return this;
	}

	public Gossips to(String personName) {
		stateOfTo.to(persons.get(personName));
		return this;
	}

	public Gossips say(String message) {
		this.stateOfTo = new StateSayTo(message);
		return this;
	}

	public String ask(String personName) {
		String message = persons.get(personName).speak();
		return message;
	}

	public void spread() {
		for (AbstractPerson person : persons.values()) {
			person.recieveMessage();
		}
		for (AbstractPerson person : persons.values()) {
			person.synchronize();
		}

	}

}
